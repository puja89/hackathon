from django.contrib import admin
from .models import *


admin.site.register([City, LocationCategory, Location,
                      Blog,LocationReview,Siteinformation,LocationGuider,Visitor])
