$(document).ready(function(){
    $('#add-int').on('click', function(){
      var int = $('#interests').val();
      $.ajax({
        url: '/add-interest/',
        method:'get',
        data: {
          'sarobar': int
        },
        success: function(){
          location.reload();
        }
      })
    });

    $('.delete-int').on('click', function(){
     	const id = $(this).data("id");
      $.ajax({
        url: "/delete-interest/",
        method:'get',
        data: {
          'sarobar': id
        },
        success: function(){
          location.reload();
        }
      })
    });

    if ('serviceWorker' in navigator) {
      console.log('CLIENT: service worker registration in progress.');
      navigator.serviceWorker.register('/static/sw.js').then(function() {
        console.log('CLIENT: service worker registration complete.');
      }, function() {
        console.log('CLIENT: service worker registration failure.');
      });
    } else {
      console.log('CLIENT: service worker is not supported.');
    }


  });