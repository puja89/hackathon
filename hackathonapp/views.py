from django.shortcuts import render, redirect
from django.views.generic import *
from .models import *
from .forms import *
from django.contrib.auth import authenticate, login, logout
from django.http import JsonResponse
from django.contrib.auth.models import User
from django.db.models import Q
from django.urls import reverse_lazy


class HomeView(TemplateView):
    template_name = 'home.html'

    def get_context_data(self, **Kwargs):
        context = super().get_context_data(**Kwargs)

        if self.request.user.is_authenticated:
            logged_user = self.request.user
            visitor = Visitor.objects.get(user=logged_user)
            interests = visitor.interests.all()
            locations =[visitor for visitor in interests]
            locationvisited=[visitor.id for visitor in visitor.interests.all()]
            
            context['categories'] = locations

            return context

class RegistrationView(CreateView):
    template_name = "registration.html"
    form_class = RegistrationForm
    success_url = reverse_lazy('hackathonapp:login')


    def form_valid(self, form):
        a = form.cleaned_data['username']
        b = form.cleaned_data['password']
        c = form.cleaned_data['email']
        puja_user = User.objects.create_user(a, c, b)
        form.instance.user = puja_user
        # form.instance.interests=puja_user.interests
        return super().form_valid(form)

        


class LoginView(FormView):
    template_name = "login.html"
    form_class = LoginForm
    success_url = reverse_lazy('hackathonapp:home')

    def form_valid(self, form):
        uname = form.cleaned_data["username"]
        pword = form.cleaned_data["password"]

        user = authenticate(username=uname, password=pword)

        if user is not None:
            login(self.request, user)
        else:
            return render(self.request, "templates/login.html", {
                "form": form,
                "error": "Invalid username or password"
            })

        return super().form_valid(form)

class LogoutView(View):
    def get(self, request):
        logout(request)
        return redirect('/')    

class LocationCategoryView(TemplateView):
    template_name = "locationcategory.html"

    def get_context_data(self, **Kwargs):
        context = super().get_context_data(**Kwargs)

        if self.request.user.is_authenticated:
        	logged_user = self.request.user
        	visitor = Visitor.objects.get(user=logged_user)
        	interests = visitor.interests.all()
        	locations =[visitor for visitor in interests]
        	locationvisited=[visitor.id for visitor in visitor.interests.all()]
        	for location in LocationCategory.objects.all():
        		if location.id not in locationvisited:
        			locations.append(location)
        	context['categories'] = locations
        else:
        	context['categories'] = LocationCategory.objects.all().order_by("title")
        if self.request.GET and self.request.GET.get('title'):	
            title = self.request.GET.get('title')
            context['categories'] = LocationCategory.objects.filter(
                title__contains=title)
        print(context['categories'])
        return context

    # def get_context_data(self,**Kwargs):
    # 	context=super().get_context_data(**Kwargs)

    # 	logged_user = self.request.user

 

class LocationListView(DetailView):
    template_name="location.html"
    model=LocationCategory
    context_object_name="location"

    def get_context_data(self,**kwargs):
        context=super().get_context_data(**kwargs)

        locationcategory=self.object 
        context['locations']=Location.objects.filter(category=locationcategory)

        return context    

class LocationDetailView(DetailView):
	template_name="locationdetail.html"
	model=Location
	context_object_name="locationlist"

class ProfileView(TemplateView):
    template_name="profile.html"
    

    def dispatch(self, request, *args, **kwargs):
        user = request.user
        print(user.groups.first())
        if user.is_authenticated and user.groups.first().name == 'visitor':
            pass
        else:
            return redirect('/login/')
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        logged_user = self.request.user
        visitor = Visitor.objects.get(user=logged_user)
        interests = visitor.interests.all()
        locations=[]
        locationvisited=[visitor.id for visitor in visitor.interests.all()]
        for location in LocationCategory.objects.all():
        	if location.id not in locationvisited:
        		locations.append(location)
        context['cats'] = locations;
        context['visitor'] = visitor
        context['interests'] = interests

        return context


class AddInterestView(View):
    def get(self, request, *args, **kwargs):
        pk = request.GET.get('sarobar')
        obj = LocationCategory.objects.get(id=pk)
        visitor = request.user.visitor
        visitor.interests.add(obj)
        return JsonResponse({'message': 'message'})

class DeleteInterestView(View):
    def get(self, request, *args, **kwargs):
        
        pk = request.GET.get('sarobar')
        obj = LocationCategory.objects.get(id=pk)
        visitor = request.user.visitor
        visitor.interests.remove(obj)
        return JsonResponse({'message': 'message'})