from django.urls import path
from .views import *

app_name = 'hackathonapp'
urlpatterns = [
    path('', HomeView.as_view(), name="home"),
    path('register/', RegistrationView.as_view(), name="registration"),
    path('login/', LoginView.as_view(), name="login"),
    path('logout/', LogoutView.as_view(), name="logout"),
    path('locationcategory/', LocationCategoryView.as_view(), name="locationcategory"),
    path('locationdetail/<int:pk>/detail/', LocationListView.as_view(), name="locationlist"),
    path('location/<int:pk>/detail/', LocationDetailView.as_view(), name="locationdetail"),
    path('profile/',ProfileView.as_view(),name="profile"),
    path('add-interest/', AddInterestView.as_view(), name="addinterest"),
    path('delete-interest/',DeleteInterestView.as_view(),name="delete-category")

]
